# C-GApps

![Logo](https://github.com/AlexLartsev19/c-gapps/blob/android-6.0/logo.png)

Information
-------------------

These are Google Apps for those who want to install Google Packages on a custom rom.
Remember Apks and Jars files are prebuilt from Google.
All closed source files come from Nexus's factory images.
This contains just the core files needed to setup a fully working Google account,
users will choose which apps they want on their devices.
GApps contain a "quite old" universal Play Service, so the Play Store will download the proper one
for your device asap.
These GApps get weekly updates, feel free to fork and contribute to this, but remember,
Opensource does not mean out-of-respect.
Also NO MIRRORS ALLOWED.

Links
-------------------

[Latest C-GApps releases](https://github.com/AlexLartsev19/c-gapps/releases/latest)

[All C-GApps releases](https://github.com/AlexLartsev19/c-gapps/releases)

[Xda Forum C-GApps Development Thread](http://forum.xda-developers.com/android/software/gapps-cgapps-unofficial-t3264174)

Credits
-------------------

Thanks to:

- [Google](https://developers.google.com/)
- [CyanogenMod](https://github.com/CyanogenMod)
- [CGApps](https://github.com/cgapps/vendor_google)
- [OpenGApps](https://github.com/opengapps/opengapps)
- [BaNks_Dynamic_GApps](https://github.com/MrBaNkS/banks_dynamic_gapps)
- [slim_gapps](https://github.com/dankoman30/slim_gapps)
- [APKMirror.com](http://www.apkmirror.com/)

Build
-------------------

You can compile C-GApps packages with GNU make

_make clean_
- Remove output directory

_make arm_
- Compile C-GApps package for arm architicture

_make arm64_
- Compile C-GApps package for arm64 architicture

_make x86_
- Compile C-GApps package for x86 architicture

_make x86_64_
- Compile C-GApps package for x86_64 architicture

_make all_
- Compile C-GApps packages for all supported architictures(arm ,arm64, x86, x86_64)
