#!/sbin/sh

#    This file is part of The C-GApps script of @Alexander Lartsev.
#
#    The C-GApps scripts are free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    These scripts are distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    CGApps sources are used with permission, under the license that it may be re-used to continue the GApps package.
#    This C-GApps Shell Script includes code derived from CGApps work of @Joey Rizzoli,
#    The CGApps are available under the GPLv2 from https://github.com/cgapps/vendor_google/tree/builds
#

# /system/addon.d/31-gapps.sh
#
. /tmp/backuptool.functions

list_files() {
cat <<EOF
lib/libfacelock_jni.so
lib/libfilterpack_facedetect.so
lib/libjni_latinime.so
lib/libjni_latinimegoogle.so
priv-app/PrebuiltGmsCore/PrebuiltGmsCore.apk
priv-app/PrebuiltGmsCore/lib/arm/libAppDataSearch.so
priv-app/PrebuiltGmsCore/lib/arm/libconscrypt_gmscore_jni.so
priv-app/PrebuiltGmsCore/lib/arm/libgcastv2_base.so
priv-app/PrebuiltGmsCore/lib/arm/libgcastv2_support.so
priv-app/PrebuiltGmsCore/lib/arm/libgmscore.so
priv-app/PrebuiltGmsCore/lib/arm/libgoogle-ocrclient-v3.so
priv-app/PrebuiltGmsCore/lib/arm/libjgcastservice.so
priv-app/PrebuiltGmsCore/lib/arm/libleveldbjni.so
priv-app/PrebuiltGmsCore/lib/arm/linNearbyApp.so
priv-app/PrebuiltGmsCore/lib/arm/libsslwrapper_jni.so
priv-app/PrebuiltGmsCore/lib/arm/libwearable-selector.so
priv-app/PrebuiltGmsCore/lib/arm/libWhisperDev.so
priv-app/Velvet/Velvet.apk
priv-app/Velvet/lib/arm/librotli.so
priv-app/Velvet/lib/arm/libcronet.so
priv-app/Velvet/lib/arm/libframesequence.so
priv-app/Velvet/lib/arm/libgoogle_speech_jni.so
priv-app/Velvet/lib/arm/libgoogle_speech_micro_jni.so
priv-app/Velvet/lib/arm/libnativecrashreporter.so
priv-app/Velvet/lib/arm/liboffline_actions_jni.so
priv-app/Velvet/lib/arm/libthird_party_brotil_dec_jni.so
EOF
}

case "$1" in
  backup)
    list_files | while read FILE DUMMY; do
      backup_file $S/$FILE
    done
  ;;
  restore)
    list_files | while read FILE REPLACEMENT; do
      R=""
      [ -n "$REPLACEMENT" ] && R="$S/$REPLACEMENT"
      [ -f "$C/$S/$FILE" ] && restore_file $S/$FILE $R
    done
  ;;
  pre-backup)
    # Stub
  ;;
  post-backup)
    # Stub
  ;;
  pre-restore)
    # Stub
  ;;
  post-restore)
    # Stub
  ;;
esac
